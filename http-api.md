<!-- very much not complete -->
# Tetrio HTTP API
*This document is a work in progress*

Note: With the 5.0.0 update, a public API has been created documented at [https://tetr.io/about/api/](https://tetr.io/about/api/) with a warning to not use the old API at risk of losing your account (bots notwithstanding).

# Endpoints
Endpoints marked as 'requires authentication' must have a token passed in for
the `Authorization: Bearer ${token}` header.

## /api/users/:username/resolve
Requires authentication
```yaml
success: boolean
_id?: string # Only if successful
errors?: # Only if not successful
  - msg: string
```

## /api/users/:userId
Requires authentication
```yaml
success: boolean
errors?: # Only if not successful
  - msg: string
user?: # only if successful
  _id: string
  username: string
  role: string # Seen as 'user', 'admin'
  ts: string # join date
  badges:
    - id: string
      label: string
      ts?: string # date, notably missing on infdev badge
  xp: number
  gamesplayed: number
  gameswon: number
  gamestime: number # very high precision seconds
  country: string # nullable
  record:
    40l:
      record: Record
      rank: number # nullable if not on leaderboard
    blitz:
      record: Record
      rank: number # nullable if not on leaderboard
  supporter: boolean
  verified: boolean
  league:
    gamesplayed: number
    gameswon: number
    rating: number # TR, -1 if anon or unranked
    glicko?: number # Missing if anon or qualifiers incomplete
    rd?: number # Missing if anon or qualifiers incomplete
    rank: string # s- s s+ etc. z if unranked
    apm?: number # missing if anon or no games played
    pps?: number
    vs?: number
    standing: number # leaderboard spot. -1 if unranked
```

## Types

### Record
```yaml
_id: string
stream: string
replayid: string
user:
  _id: string
  username: string
ts: string # replay date
endcontext:
  seed: number
  lines: number
  level_lines: number
  level_lines_needed: number
  inputs: number
  time:
    start: number
    zero: boolean
    locked: boolean
    prev: number
    frameoffset: number
  score: number
  zenlevel: number
  zenprogress: number
  level: number
  combo: number
  currentcombopower: number
  topcombo: number
  btb: number
  topbtb: number
  tspins: number
  piecesplaced: number
  clears:
    singles: number
    doubles: number
    triples: number
    quads: number
    realtspins: number
    minitspins: number
    minitspinsingles: number
    tspinsingles: number
    minitspindoubles: number
    tspindoubles: number
    tspintriples: number
    tspinquads: number
    allclear: number
  garbage:
    sent: number
    received: number
    attack: number
    cleared: number
  kills: number
  finesse:
    combo: number
    faults: number
    perfectpieces: number
  finalTime: number
  gametype: string # '40l' or 'blitz'
```
