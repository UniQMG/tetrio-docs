# Tetrio API
*This document is a work in progress*

WebSocket url: `wss://tetr.io/ribbon`

Packets take the form of `{ command: string, data: object }` where the command
is a section below and the data payload is an object documented below. Packets
after the initial handshake also have an `{ id: number }` parameter used
internally by Ribbon.js

A '?' after an attribute indicates that it's optional. Optional attributes are
not defined at all, rather than just null.

<!-- TODO: document other ribbonjs builtin payloads -->

# Client commands (outbound packets)

## ping
Handled automatically by Ribbon.js
No payload

## authorize
```yaml
handling:
  arr: string
  das: string
  sdf: string
  safelock: boolean
signature: # find this at the top of tetrio.js
  build:
    id: string
    time: number
  commit:
    id: string
    time: number
  countdown: boolean
  mode: string,
  serverCycle: string,
  version: string
token: string # find this at localStorage.userToken
```

## createroom
```yaml
string # Seen as 'private' or 'public'
```

## joinroom
```yaml
string # Room ID
```

## leaveroom
```yaml
boolean # Unknown?
```

## chat
```yaml
string # message content
```

## startroom
No payload

## switchbracket
```yaml
string # Seen as 'spectator', 'player'
```

## switchbrackethost
```yaml
uid: string # Looks like a player id
bracket: string # Seen as 'spectator', 'player'
```

## transferownership
```yaml
string # User ID
```

## takeownership
No payload. Takes ownership of a room you created.

## updateconfig
```yaml
# Note this is an array of { index, value } entries
- index: string # Attribute name, e.g. 'meta.name'
  value: string
```

# Server commands (inbound packets)

## authorize
```yaml
maintenance: boolean
success: boolean
```

## kick
```yaml
reason: string
```

## pong
No payload

## err
```yaml
string # Seen as 'You are already in this room'
```

## chat
```yaml
content: string
content_safe?: string # Only if not system message. Word-filtered content
system: boolean
user:
  _id: string
  username: string
  role?: string # Only if not system message. Seen as 'user', 'anon', 'bot'
  supporter?: boolean # Only if not system message
  verified?: boolean # Only if not system message
```

## whisper
```yaml
msg: string
color: string # hex code including hash
icon: string # Seen as 'swaphost'
```

## joinroom
```yaml
string # room ID
```

## leaveroom
```yaml
string # ID of the room left
```

## gmupdate
Sent any time something about the room updates.
e.g. settings are changed, the game starts, or a player joins.
```yaml
auto:
  maxtime: number
  status: string # Seen as 'off'
  time: number
game:
  state: string # seen as 'lobby'
  options: GameOptions
  match:
    type: string # Seen as 'versus'
    ft: number
    gamemode: string # Seen as 'VERSUS'
    winningKey: string # Seen as 'STAY_ALIVE'
    keys:
      primary: string # seen as 'WINS'
      primaryLabel: string # seen as 'POINTS'
      primaryLabelSingle: string # seen as 'POINT'
      primaryIsAvg: string # seen as false
      secondary: string # seen as 'stats.garbage.sent'
      secondaryLabel: string # seen as 'LINES SENT'
      secondaryLabelSingle: string # seen as 'LINE SENT'
      secondaryIsAvg: boolean # seen as false
      tertiary: string # seen as 'stats.garbage.received'
      tertiaryLabel: string # seen as 'LINES RECEIVED'
      tertiaryLabelSingle: string # seen as 'LINE RECEIVED'
      tertiaryIsAvg: boolean # seen as false
    extra: {} # Unknown
id: string
meta:
  name: string
  userlimit: number # 0 for unlimited
  allowAnonymous: boolean
  allowBots: boolean
  bgm: string
  match:
    type: string # versus
    ft: number
owner: string # player id of current room hots
players: # unbounded array
  - _id: string
    username: string
    anon: boolean
    bot: boolean
    xp: number
    record:
      games: 0
      wins: 0
      streak: 0
    bracket: string # 'player' 'spectator'
    supporter: boolean
    verified: boolean
    country: string # nullable
type: string # Seen as 'private' or 'public'
```

## readymulti
```yaml
options: GameOptions
contexts: # One per player
  - listenID: string
    user:
      _id: string
      username: string
    handling:
      arr: number
      das: number
      sdf: number
      safelock: boolean
    first: boolean
    gameID: string # number-format
```

## refereeboard
```yaml
refereedata:
  ft: 1
  gamemode: string # Seen as 'VERSUS'
leaderboard: # One per player
  - TODO
```

## startmulti
No payload

## endmulti
<!-- TODO: Differentiate between currentboard and leaderboard? -->
```yaml
leaderboard: LeaderboardEntry[]
currentboard:
  - user:
      _id: string # user id
      username: string
    active: boolean
    success: boolean # Whether the player succeeded in the round
    winning: number
xpPerUser: 124
```



# Shared commands (both directions)

## replay
```yaml
listenID: string # looks like a player id
frames:
  - frame: number
    type: string
    data: FrameData
provisioned: number # Unknown?
```

# Types

## FrameData
This differs depending on the frame type

### type: start
Inbound and outbound.
```yaml
  {}
```

### type: keyup
Outbound only
```yaml
key: string # moveLeft moveRight hardDrop softDrop rotateCW rotateCCW hold rotate180
subframe: number # 0 to 1
```

### type: keydown
Outbound only
```yaml
key: string # moveLeft moveRight hardDrop softDrop rotateCW rotateCCW hold rotate180
subframe: number # 0 to 1
```

### type: ige
Inbound only. Looks like it has a nested frame?
```yaml
id: number
frame: number
type: "ige"
data:
  type: string # Seen as 'attack'
  lines: number
  column: number
  sender: string # username of attacker

```

### type: targets
Inbound and outbound. Looks like it has a nested frame?
```yaml
id: string # Seen as "diyusi"
frame: number
type: string # Seen as "targets"
data:
  - string # looks like a player ID
```

### type: end
Inbound only. Emitted whenever a player dies.
```yaml
reason: string # Seen as 'garbagesmash'
export: GameExports
```

### type: full
Outbound only. Seen every 300 frames, including at the very start.
```yaml
GameExports
```

## GameExports
```yaml
successful: false
gameoverreason: string # Nullable, seen as null
replay: {} # Possibly only used during an actual replay and not a live game
source: {} # Seems to be used internally and stripped before sending over net
options: GameOptions
stats: GameStats
targets: [] # Unknown, probably targeting related
fire: number
game:
  board: [[string]] # 40x10 array, filled with null or a piece string
                    # Piece strings appear to be different than from maps
                    # Seen as 'gb'
  bag: [string] # filled with 1-char piece strings
  hold:
    piece: string # nullable
    locked: boolean
  g: number
  controlling:
    ldas: number
    ldasiter: number
    lshift: boolean
    rdas: number
    rdasiter: number
    rshift: boolean
    lastshift: number
    softdrop: boolean
  handling:
    arr: number
    das: number
    sdf: number
    safelock: boolean
  playing: boolean
killer:
  name: string # nullable
  type: string # Seen as 'sizzle' when not dead yet
assumptions: {} # Unknown?
aggregatestats:
  apm: number
  pps: number
  vsscore: number
```

## LeaderboardEntry
```yaml
naturalorder: number # incrementing number
user:
  _id: string
  username: string
active: boolean
wins: number
points:
  primary: number
  secondary: number
  tertiary: number
  extra: {} # Unknown
inputs: number
```

## GameStats
```yaml
seed: number
lines: number
level_lines: number
level_lines_needed: number
inputs: number
time:
  start: number # Seen starting at 0
  zero: boolean
  locked: boolean
  prev: number
  frameoffset: number
score: number
zenlevel: number
zenprogress: number
level: number
combo: number
currentcombopower: number
topcombo: number
btb: number
topbtb: number
tspins: number
piecesplaced: number
clears:
  singles: number
  doubles: number
  triples: number
  quads: number
  realtspins: number
  minitspins: number
  minitspinsingles: number
  tspinsingles: number
  minitspindoubles: number
  tspindoubles: number
  tspintriples: number
  tspinquads: number
  allclear: number
garbage:
  sent: number
  received: number
  attack: number
  cleared: number
kills: number
finesse:
  combo: number
  faults: number
  perfectpieces: number
```

## GameOptions
```yaml
version: number # seen as 15
seed_random: boolean
seed: number
g: number # gravity
stock: 0
countdown: boolean
countdown_count: number
countdown_interval: number
precountdown: number
prestart: number
mission: string # User configurable room text
mission_type: string # seen as 'mission_versus'
zoominto: string # Seen as 'slow'
display_lines: boolean
display_attack: boolean
display_pieces: boolean
display_impending: boolean
display_kills: boolean
display_placement: boolean
display_fire: boolean
hasgarbage: boolean
neverstopbgm: boolean
display_next: boolean
display_hold: boolean
gmargin: number
gincrease: number
garbagemultiplier: number
garbagemargin: number
garbageincrease: number
garbagecap: number
garbagecapincrease: number
garbagecapmax: number
bagtype: string # Seen as '7bag'
spinbonuses: string # Seen as 'T-spins'
kickset: string # Seen as 'SRS'
nextcount: number
allow_harddrop: boolean
display_shadow: boolean
locktime: number
garbagespeed: number
forfeit_time: number
are: number
lineclear_are: number
infinitemovement: boolean
lockresets: number
allow180: boolean
objective:
  type: string # Seen as 'none'
room_handling: boolean
room_handling_arr: number
room_handling_das: number
room_handling_sdf: number
manual_allowed: boolean
b2bchaining: boolean
scalegarbagespeed: boolean
clutch: boolean
# Only seen in multiplayer replay data so far
display_stopwatch?: boolean
display_vs?: boolean
username?: string
physical?: boolean
```
